﻿/*
 *  date: 2018-10-28
 *  author: John-chen
 *  cn: 所有协议的事件
 *  en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// 所有协议的事件
    /// </summary>
    public class NetEventId
    {
        /// <summary>
        /// protobuf事件
        /// </summary>
        public const string PROTOBUFEVENT = "PROTOBUFEVENT";

        /// <summary>
        /// 获取protobuf的实例事件
        /// </summary>
        /// <param name="id"> 协议ID </param>
        /// <returns></returns>
        public static string GetProtobufInsEvent(int id)
        {
            string eventName = PROTOBUFEVENT + id;

            return eventName;
        }
    }
}