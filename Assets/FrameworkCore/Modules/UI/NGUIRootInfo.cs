﻿/*
 *  date: 2018-08-19
 *  author: John-chen
 *  cn: NGUI 的 UIRoot的信息
 *  en: todo:
 */

using UnityEngine;

namespace JyFramework
{
    /// <summary>
    /// NGUI 的根节点信息
    /// </summary>
    public class NGUIRootInfo : UIRootInfo
    {
        public NGUIRootInfo(GameObject rootObj, VoidDelegateSetWindow setWindowFunc) :base(rootObj, setWindowFunc)
        {
            
        }
    }
}