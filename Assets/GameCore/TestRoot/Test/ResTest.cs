﻿using GameCore.BaseCore;
using JyFramework;
using UnityEngine;

namespace GameCore.Test
{
    public class ResTest
    {
        /// <summary>
        /// 带参构造
        /// </summary>
        /// <param name="loader"></param>
        public ResTest()
        {
            resMgr = ResMgr.Ins;
        }

        /// <summary>
        /// 创建UI根节点
        /// </summary>
        public void TestLoadUIRoot()
        {
            var ya = new YieldArgs(uiRoot);
            MonoHelper.Ins.StartCoroutine(resMgr.AsyncLoadRes(ya, ab => { Debug.Log("Loaded uiroot ab res"); }));
            MonoHelper.Ins.StartCoroutine(resMgr.AsyncLoadRes(ya, ab => { }));

            MonoHelper.Ins.StartCoroutine(resMgr.AsyncInstantiate(ya, (obj) =>
            {
                Debug.Log("Loaded uiroot gameObject");

                obj.transform.parent = GameApp.transform;

                obj.transform.localPosition = Vector3.left * 10000;
                obj.transform.localScale = Vector3.one;
                obj.transform.localEulerAngles = Vector3.zero;
                obj.name = "UIRoot";

                parent = obj.transform.Find("CanvasRoot");
            }));
            
        }

        /// <summary>
        /// 创建其他UI对象
        /// </summary>
        public void TestLoadPrefab3()
        {
            var ya = new YieldArgs(p3Name);
            MonoHelper.Ins.StartCoroutine(resMgr.AsyncLoadRes(ya, ab => { }));

            MonoHelper.Ins.StartCoroutine(resMgr.AsyncInstantiate(ya, (obj) =>
            {
                p1 = obj;
                ResetParent(parent, obj.transform);
            }));

            MonoHelper.Ins.StartCoroutine(resMgr.AsyncInstantiate(ya, (obj) =>
            {
                p2 = obj;
                ResetParent(parent, obj.transform);
            }));

            MonoHelper.Ins.StartCoroutine(resMgr.AsyncInstantiate(ya, (obj) =>
            {
                p3 = obj;
                ResetParent(parent, obj.transform);
            }));

            MonoHelper.Ins.StartCoroutine(resMgr.AsyncInstantiate(ya, (obj) =>
            {
                p4 = obj;
                ResetParent(parent, obj.transform);
            }));

            MonoHelper.Ins.StartCoroutine(resMgr.AsyncInstantiate(ya, (obj) =>
            {
                p5 = obj;
                ResetParent(parent, obj.transform);
            }));
        }

        /// <summary>
        /// 设置父节点
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="child"></param>
        private void ResetParent(Transform parent, Transform child)
        {
            if (parent == null || child == null) return;

            child.SetParent(parent);
            ResetTransform(child);
        }

        /// <summary>
        /// 重设对象
        /// </summary>
        /// <param name="tran"></param>
        private void ResetTransform(Transform tran)
        {
            tran.localPosition = Vector3.zero;
            tran.localScale = Vector3.one;
            //tran.Rotate(Vector3.zero);
            //tran.localRotation = Quaternion.Euler(Vector3.zero);
            tran.localEulerAngles = Vector3.zero;
        }

        /// <summary>
        /// 更新
        /// </summary>
        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                GameObject.Destroy(p1);
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                GameObject.Destroy(p2);
            }
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                GameObject.Destroy(p3);
            }

            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                GameObject.Destroy(p4);
            }

            if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                GameObject.Destroy(p5);
            }
            Debug.Log("Res Test update!");
        }

        private string p3Name = "resourcescore/_ui/panel3.prefab.jyassetbundle";
        private string uiRoot = "resourcescore/_ui/uiroot.prefab.jyassetbundle";
        private Transform parent;
        private ResMgr resMgr;
        private GameObject p1, p2, p3, p4, p5;
    }
}